﻿using System.Threading.Tasks;
using MediatR;

namespace EventBus
{
    /// <summary>
    /// 事件总线服务
    /// </summary>
    public class EventBusMemoryBus : IEventBus
    {
        private readonly IMediator _mediator;

        /// <summary>
        ///
        /// </summary>
        /// <param name="mediator"></param>
        public EventBusMemoryBus(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// 异步发布事件
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="eventObj"></param>
        /// <returns></returns>
        public Task PublishAsync<T>(T eventObj) where T : IEvent
        {
            return _mediator.Publish(eventObj);
        }

        /// <summary>
        /// 发布事件同步等待
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="eventObj"></param>
        void IEventBus.Publish<T>(T eventObj)
        {
            _mediator.Publish(eventObj).GetAwaiter().GetResult();
        }
    }
}