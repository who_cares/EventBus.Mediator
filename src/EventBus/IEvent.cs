﻿using MediatR;

namespace EventBus
{
    /// <summary>
    /// 事件类接口
    /// </summary>
    public interface IEvent : INotification
    {
    }
}