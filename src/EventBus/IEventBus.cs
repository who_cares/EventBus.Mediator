﻿using System.Threading.Tasks;

namespace EventBus
{
    /// <summary>
    /// 事件总线
    /// </summary>
    public interface IEventBus
    {
        /// <summary>
        /// 异步发布事件
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="eventObj"></param>
        /// <returns></returns>
        Task PublishAsync<T>(T eventObj) where T : IEvent;

        /// <summary>
        /// 发布事件同步等待
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="eventObj"></param>
        void Publish<T>(T eventObj) where T : IEvent;
    }
}