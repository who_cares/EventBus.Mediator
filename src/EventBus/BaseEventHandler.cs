﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;

namespace EventBus
{
    /// <summary>
    /// 事件处理基类
    /// </summary>
    /// <typeparam name="TEvent"></typeparam>
    public abstract class BaseEventHandler<TEvent> : INotificationHandler<TEvent> where TEvent : IEvent
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        public virtual Task HandleAsync(TEvent e)
        {
            Handle(e);
            return Task.CompletedTask;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        public virtual void Handle(TEvent e)
        {

        }

        Task INotificationHandler<TEvent>.Handle(TEvent notification, CancellationToken cancellationToken)
        {
            return HandleAsync(notification);
        }
    }
}