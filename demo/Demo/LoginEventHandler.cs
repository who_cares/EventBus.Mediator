﻿using System;
using EventBus;

namespace Sample
{
    public class LoginEventHandler : BaseEventHandler<LoginEvent>
    {
        public override void Handle(LoginEvent e)
        {
            Console.WriteLine($"{e.UserId} login success: {e.Success}");
        }
    }
}