﻿using System;
using EventBus;
using Microsoft.Extensions.Hosting;
using System.Threading;
using System.Threading.Tasks;

namespace Sample
{
    public class TestService : BackgroundService
    {
        private readonly IEventBus _eventBus;

        public TestService(IEventBus eventBus)
        {
            _eventBus = eventBus;
        }
        
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            await Task.CompletedTask;
            while (!stoppingToken.IsCancellationRequested)
            {
                var now = DateTime.Now;
                await _eventBus.PublishAsync(new LoginEvent { UserId = now.Second, Success = (now.Second % 2) > 0 });
                await Task.Delay(1000, stoppingToken);
            }
        }
    }
}
