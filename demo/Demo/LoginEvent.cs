﻿using EventBus;

namespace Sample
{
    public class LoginEvent : IEvent
    {
        public int UserId { get; set; }

        public bool Success { get; set; }
    }
}